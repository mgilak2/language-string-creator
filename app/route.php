<?php
Route::get("/",["controller"=>"LanguageString\\LanguageStringController@index"]);

Route::get("get-file-info",["controller"=>"LanguageString\\LanguageStringController@getFilesInfo"]);

Route::get("get-file-lines",["controller"=>"LanguageString\\LanguageStringController@getSingleFileLines"]);

Route::get("create-file",["controller"=>"LanguageString\\LanguageStringController@createFile"]);
Route::get("delete-file",["controller"=>"LanguageString\\LanguageStringController@deleteFile"]);
Route::post("update-file",["controller"=>"LanguageString\\LanguageStringController@updateFile"]);

Route::get("test",['function'=>function(){
    $LSC = new LanguageStringCreator;
    $LSC->update("");
}]);

//Route::get("add-row",["controller"=>"LanguageString\\LanguageStringController@addRow"]);

