<!doctype html>
<html lang="en" ng-app="ThemeTranslator">
<head>
    <meta charset="UTF-8">
    <title>translation app</title>
    <link rel="stylesheet" href="assets/css/lib/bootstrap.css"/>
    <link rel="stylesheet" href="assets/css/style.css"/>
</head>
<body ng-controller="Files">

<div class="container" >
    <div class="page-header">
        <h1>Example page header <small>Subtext for header</small></h1>
    </div>
    <div class="row">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">
            create +
        </button>
<!--        <button class="btn btn-success">create +</button>-->

        <div class="well">
            <ul class="list-group">
                <translation-file ng-repeat="file in files" file="file"></translation-file>
            </ul>
        </div>
    </div>

</div>

<div class="container" >
    <div class="row">
        <button ng-click="save()" class="btn btn-success">save</button>
        <button ng-click="addRow()" class="btn btn-success">add</button>
        <span>{{filename}}</span>
    <div class="well">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>string</th>
                <th>translate</th>
                <th>remove</th>
            </tr>
            </thead>
            <tbody>
                <tr translation-row ng-repeat="row in rows" row="row" index="{{$index}}"></tr>
            </tbody>
        </table>

    </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <label for="">file name</label>
                <input type="text" ng-model="filename" class="form-control"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button ng-click="createFile(filename)" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/lib/jquery-2.1.3.min.js"></script>
<script src="assets/js/lib/bootstrap.min.js"></script>
<script src="assets/js/lib/angular.min.js"></script>
<script src="ng-app/app.js"></script>
<script src="ng-app/directives/translationFile.js"></script>
<script src="ng-app/directives/translationRow.js"></script>
</body>
</html>