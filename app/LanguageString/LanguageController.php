<?php namespace LanguageString;

class LanguageStringController
{
    protected $baseDir;

    protected $Translator;
    public function __construct()
    {
        $this->Translator = new LanguageStringCreator;
        $this->baseDir = __DIR__."/../../languages";
    }

    public function index()
    {
        include __DIR__."/../views/view.php";
    }

    public function getFilesInfo()
    {
        $files         = $this->Translator->getTranslationFiles($this->baseDir);
        $filesStrings  = $this->generateAngulerishJsonForFiles($files);

        echo($filesStrings);
    }

    public function getSingleFileLines()
    {
        $rows = $this->Translator
            ->open($this->baseDir."/".$_GET['fileName'])
            ->readIntoArray()
            ->getLines();

        $rows = $this->generateAngulerishJsonForRows($rows);

        echo $rows;
    }

    public function createFile()
    {
        $fileName = $_GET['fileName'];
        $this->Translator->createTranslationFile($fileName,$this->baseDir);
    }

    public function deleteFile()
    {
        $fileName = $_GET['fileName'];
        $this->Translator->deleteTranslationFile($fileName,$this->baseDir);
    }

    public function updateFile()
    {
        $data = json_decode(file_get_contents('php://input'));
        $rows = [];
        foreach($data->rows as $row)
        {
           $rows[] = [$row->string,$row->translate];
        }

        $this->Translator
            ->cleanOut($this->baseDir."/".$data->file)
            ->writeOutOfArray($rows);
    }

    private function generateAngulerishJsonForFiles($items)
    {
        $itemsStrings = "";
        $itemsStrings .= "[";
        foreach($items as $item)
        {
            $itemsStrings .= '{"name":"'.$item.'"},';
        }
        $itemsStrings = substr($itemsStrings,0,strlen($itemsStrings)-1);
        $itemsStrings .= "]";

        return $itemsStrings;
    }

    private function generateAngulerishJsonForRows($items)
    {
        $itemsStrings = "";
        $itemsStrings .= "[";
        foreach($items as $item)
        {
            $itemsStrings .= '{"string":"'.$item[0].'","translate":"'.$item[1].'"},';
        }
        $itemsStrings = substr($itemsStrings,0,strlen($itemsStrings)-1);
        $itemsStrings .= "]";
        if(count($items) == 0)
        {
            $itemsStrings = '[{"string":"","translate":""}]';
        }
        return $itemsStrings;
    }
}