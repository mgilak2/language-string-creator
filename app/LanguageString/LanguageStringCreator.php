<?php namespace LanguageString;

class LanguageStringCreator
{
    protected $direction;

    protected $file;

    protected $lines = [];

    protected $languagesDirectory = '/../languages';

    public function setFile($direction)
    {
        $this->direction = $direction;

        $this->file = fopen($direction, "rw");

        return $this;
    }

    public function open($direction)
    {
        $this->direction = $direction;

        $this->lines = file( $this->direction , FILE_IGNORE_NEW_LINES );

        return $this;
    }

    public function readIntoArray($direction = null)
    {
        if(is_null($direction))
        {
            $direction = $this->direction;
        }

        $this->open($direction);

        $temps = [];

        foreach($this->lines as $line)
        {
            $temps[] = explode("=",$line);
        }

        $this->lines = $temps;

        return $this;
    }

    public function writeOutOfArray(array $lines = [],$direction = null)
    {
        if(!is_null($direction))
        {
            $this->direction = $direction;
        }

        $this->open($this->direction);

        $this->lines = $lines;
        foreach ($lines as $line)
        {
            fwrite($this->file, trim($line[0]) . " = " .trim($line[1]). "\n");
        }

        return $this;
    }

    public function cleanOut($direction)
    {
        $this->direction = $direction;

        $this->file = fopen ($direction, "w+");

        $this->lines = file( $this->direction , FILE_IGNORE_NEW_LINES );

        return $this;
    }

    public function add($item)
    {
        $file = fopen($this->direction,"a+");

        fwrite($file, trim($item[0]) . " = " .trim($item[1]). "\n");

        fclose($file);

        $this->lines = file( $this->direction , FILE_IGNORE_NEW_LINES );

        return $this;
    }

    public function update($line_number,$data)
    {
        $lines = file( $this->direction , FILE_IGNORE_NEW_LINES );

        $lines[$line_number] = $data[0]." = ".$data[1];

        file_put_contents( $this->direction , implode( "\n", $lines ) );

        $this->lines = file( $this->direction , FILE_IGNORE_NEW_LINES );

        return $this;
    }

    public function delete($line_number)
    {
        $lines = file( $this->direction , FILE_IGNORE_NEW_LINES );

        $lines[$line_number] = null;

        $temp = [];

        foreach($lines as $line)
        {
            if(!is_null($line))
            {
                $temp[] = $line;
            }
        }

        file_put_contents( $this->direction , implode( "\n", $temp ) );

        $this->lines = file( $this->direction , FILE_IGNORE_NEW_LINES );

        return $this;
    }

    public function getLines()
    {
        return $this->lines;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function getDirection()
    {
        return $this->direction;
    }

    public function getTranslationFile($filename,$directory = null)
    {
        if(! is_null($directory))
        {
            $this->languagesDirectory = $directory;
        }

        if(file_exists(__DIR__.$this->languagesDirectory."/".$filename.".txt"))
        {
            return __DIR__.$this->languagesDirectory."/".$filename.".txt";
        }

        return false;
    }

    public function getTranslationFiles($directory)
    {
        $translateFiles = [];

        if ($handle = opendir($directory))
        {
            while (false !== ($entry = readdir($handle)))
            {
                if ($entry != "." && $entry != "..")
                {
                    $translateFiles[] = $entry;
                }
            }

            closedir($handle);
        }

        return $translateFiles;
    }

    public function createTranslationFile($filename,$directory = null)
    {
        if(! is_null($directory))
        {
            $this->direction = $directory;
        }

        $translationFile = fopen($this->direction."/".$filename.".txt", "w") or die("Unable to open file!");

        fclose($translationFile);
    }

    public function deleteTranslationFile($filename,$directory = null)
    {
        if(! is_null($directory))
        {
            $this->direction = $directory;
        }

        unlink($this->direction."/".$filename);
    }
}