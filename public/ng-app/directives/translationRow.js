app.directive('translationRow', function () {
    return {
        restrict:"EA",
        scope:{
            row : "=",
            index:"@"
        },
        transclude: true,
        templateUrl:"ng-app/templates/TranslationRow.html",
        compile: function (element,attrs)
        {
            return function (scope,element,attrs)
            {
                element.find("th").html(parseInt(scope.index) + 1);
                
                scope.deleteRow = function (index) {
                    var temp = [];
                    for(var i= 0;i<scope.$parent.rows.length;i++)
                    {
                        if(i != parseInt(index))
                        {
                            temp.push(scope.$parent.rows[i]);
                        }
                    }
                    scope.$parent.updateRows(temp);
                    console.log(temp);
                    //console.log(scope.$parent.rows);
                    //console.log(index);
                    //$(this).parent().parent().remove();
                }
            }
        }
    };
});