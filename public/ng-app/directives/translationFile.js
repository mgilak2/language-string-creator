app.directive('translationFile', function ($http) {
    return {
        restrict:"EA",
        scope:{
            file : "="
        },
        templateUrl:"ng-app/templates/TranslationFile.html",
        compile: function (element,attrs) {
            return function (scope,element,attrs) {

                scope.delete = function (filename)
                {
                    console.log(filename);
                    $http.get('delete-file/?fileName='+filename).
                        success(function(data, status, headers, config)
                        {
                            scope.$parent.getFileInfo();
                            //scope.$parent.$apply();
                        }).
                        error(function(data, status, headers, config)
                        {
                            console.log("failed pulling "+fileName+" lines")
                        });
                };

                scope.update = function (fileName) {
                    $http.get('get-file-lines/?fileName='+fileName).
                        success(function(data, status, headers, config)
                        {
                            scope.$parent.updateRows(data);
                            scope.$parent.setFileName(fileName);
                        }).
                        error(function(data, status, headers, config)
                        {
                            console.log("failed pulling "+fileName+" lines")
                        });
                };
            }
        }
    };
});

