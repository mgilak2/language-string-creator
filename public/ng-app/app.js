var app =  angular.module("ThemeTranslator",[]);

app.controller("Files", function ($scope,$http,$compile) {
    $scope.files = [];
    $scope.filename = "";

    $scope.getFileInfo = function () {
        $http.get('get-file-info').
            success(function(data, status, headers, config)
            {
                $scope.files = data;

                if(data.length > 0)
                {
                    $scope.filename = data[0].name;
                    $http.get('get-file-lines/?fileName='+data[0].name).
                        success(function(rows, status, headers, config)
                        {
                            $scope.rows = rows;
                        }).
                        error(function(data, status, headers, config)
                        {
                            console.log("failed pulling "+fileName+" lines")
                        });
                }

            }).
            error(function(data, status, headers, config)
            {
                console.log("failed pulling files info")
            });
    };

    $scope.getFileInfo();

    $scope.updateRows = function(rows)
    {
        $scope.rows = rows;
    };

    $scope.setFileName = function (name)
    {
      $scope.filename = name;
    };

    $scope.createFile = function (filename)
    {
        $http.get('create-file/?fileName='+filename).
            success(function(data, status, headers, config)
            {
                $scope.getFileInfo();
                //$scope.$apply();
            }).
            error(function(data, status, headers, config)
            {
                console.log("failed pulling "+fileName+" lines")
            });
    };
    $scope.newRows = [];

    $scope.addRow = function () {
        var obj = {name:"",translate:""};
        $scope.rows.push(obj);
        $scope.newRows.push(obj);
    };

    $scope.check = function () {
      console.log($scope.rows);
    };

    $scope.save = function () {
        $http.post('update-file',{rows:$scope.rows,file:$scope.filename}).
            success(function(data, status, headers, config)
            {
                //$scope.getFileInfo();
                //$scope.$apply();
            }).
            error(function(data, status, headers, config)
            {
                console.log("failed pulling "+fileName+" lines")
            });
    }
});

