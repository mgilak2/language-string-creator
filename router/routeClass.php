<?php
class Route {
    public static $strict = array("{int}"=>"{int}", "{letter}"=>"{letter}", "{number}"=>"{number}");

    public static  $get = array();

    public static $post = array();

    public $arguments = array();

    static function get($route,$options = [] )
    {
        $defaults = [ "function" => null ,"beforFilter" => null,"afterFilter" => null,"name" => null,"controller"=> [null,null],"run"=>null ];

        $options = array_merge($defaults, $options);

        Route::$get[] = [$route,$options];
    }

    static function post($route,$options = [])
    {
        $defaults = [ "function" => null ,"beforFilter" => null,"afterFilter" => null,"name" => null,"controller"=> [null,null] ];

        $options = array_merge($defaults, $options);

        self::$post[] = [$route,$options];
    }

    static function clearGeTArguments()
    {
        $requestURI = $_SERVER['REQUEST_URI'];
        if(strpos($_SERVER['REQUEST_URI'],"?"))
        {
            $requestURI = explode("?",$_SERVER['REQUEST_URI']);

            if(count($requestURI)>0)
            {
                array_pop($requestURI);
                $requestURI = implode("",$requestURI);
            }
        }

        return $requestURI;
    }

    static function matchGetRoute($Route)
    {
        $requestURI = self::clearGeTArguments();

        if($requestURI[strlen($requestURI) - 1] === "/" && strlen($requestURI)>1)
        {
            $requestURI = substr($requestURI,0,strlen($requestURI) -1);
        }

        $requestURI = explode('/', $requestURI);
        $status = true; // for breaking out of foreach -- helper for checking whether the route parts has been seen or not (profile/logo)
        // has 2 part base on / spliter . this flag ganna check to not trespassing 2 part ;)
        $routeFlag = false; // helper to point for the valid route
        $placeholder = 0; // pointer on the valid route

        foreach(self::$get as $validRoute)
        { // iterate on all get routes
            if(!$status ) break;

            if(isset($validRoute[0][0]) && $validRoute[0][0] === "/")
            {
                $validRoute[0] = substr($validRoute[0],1,strlen($validRoute[0]) -1);
            }

            if(isset($validRoute[0][strlen($validRoute[0]) -1]) && $validRoute[0][strlen($validRoute[0]) -1] === "/")
            {
                $validRoute[0] = substr($validRoute[0],0,strlen($validRoute[0]) -1);
            }


            $routePart = explode('/', $validRoute[0]); // explode get routes
            if(count($routePart) == (count($requestURI)-1) && $routeFlag == false){ // check whether parts are equal or not .
                                                                                    //also whether we find something or not
                $i=1; // request uri array has empty string on its zero place . so we have to start from place 1
                foreach($routePart as $part)
                { // this inner foreach check for equasion of route we set and the uri

                    if($part == $requestURI[$i])
                    {
                        // this if gous for litteral equasion
                        $routeFlag = true;  // if the condition passed it means we are likely found the route but yet all the parts must check
                        if((count($requestURI)-1) == $i)
                        { //this is ganna check for not trespassing parts
                            $status = false;
                            break;
                        }

                        ++$i;

                    }
                    elseif($key = array_search($part,self::$strict))
                    {
                        // this if checks for {int},{number},{letter} ...
                        switch($key)
                        {
                            case "{int}":
                                if(self::int($requestURI[$i]))
                                {
                                    $Route->arguments[] = $requestURI[$i];
                                    $routeFlag = true;
                                    if((count($requestURI)-1) == $i)
                                    {

                                        $status = false;
                                        break;
                                    }
                                    $i++;

                                }
                                else
                                {
                                    $routeFlag = false;
                                    if((count($requestURI)-1) == $i) break;
                                    $status = false;
                                }
                                break;

                            case "{letter}":
                                if(true){
                                    $Route->arguments[] = $requestURI[$i];
                                    $routeFlag = true;
                                    if((count($requestURI)-1) == $i)
                                    {
                                        $status = false;
                                        break;
                                    }
                                    $i++;
                                }else{

                                }
                                break;
                        }

                    }else{
                        if((count($requestURI)-1) == $i){
                            $routeFlag = false;
                            break;
                        }

                    }
                }
            }

            $placeholder++;
        };

        if($routeFlag)
        {
            return [self::$get[$placeholder-1],$requestURI,$Route->arguments];
        }
        else
        {
            return false;
        }

    }

    static function matchPostRoute($Route)
    {
        $requestURI = explode('/', $_SERVER['REQUEST_URI']);

        $status = true; // for breaking out of foreach -- helper for checking whether the route parts has been seen or not (profile/logo)
        // has 2 part base on / spliter . this flag ganna check to not trespassing 2 part ;)
        $routeFlag = false; // helper to point for the valid route
        $placeholder = 0; // pointer on the valid route

        foreach(self::$post as $validRoute)
        { // iterate on all post routes
            if(!$status ) break;
            $routePart = explode('/', $validRoute[0]); // explode post routes

            if(count($routePart) == (count($requestURI)-1) && $routeFlag == false){ // check whether parts are equal or not .
                //also whether we find something or not

                $i=1; // request uri array has empty string on its zero place . so we have to start from place 1
                foreach($routePart as $part)
                { // this inner foreach check for equasion of route we set and the uri

                    if($part == $requestURI[$i])
                    { // this if gous for litteral equasion
                        $routeFlag = true;  // if the condition passed it means we are likely found the route but yet all the parts must check
                        if((count($requestURI)-1) == $i)
                        { //this is ganna check for not trespassing parts
                            $status = false;
                            break;
                        }

                        ++$i;

                    }
                    elseif($key = array_search($part,self::$strict))
                    { // this if checks for {int},{number},{letter} ...
                        switch($key){
                            case "{int}":

                                if(self::int($requestURI[$i]))
                                {
                                    $Route->arguments[] = $requestURI[$i];
                                    $routeFlag = true;
                                    if((count($requestURI)-1) == $i)
                                    {

                                        $status = false;
                                        break;
                                    }
                                    $i++;

                                }
                                else
                                {
                                    $routeFlag = false;
                                    if((count($requestURI)-1) == $i) break;
                                    $status = false;
                                }
                                break;

                            case "{letter}":
                                if(true){
                                    $Route->arguments[] = $requestURI[$i];
                                    $routeFlag = true;
                                    if((count($requestURI)-1) == $i)
                                    {
                                        $status = false;
                                        break;
                                    }
                                    $i++;
                                }
                                else
                                {

                                }
                                break;
                        }

                    }
                    else
                    {
                        if((count($requestURI)-1) == $i)
                        {
                            $routeFlag = false;
                            break;
                        }

                    }
                }
            }

            $placeholder++;
        };

        if($routeFlag)
        {
            return [self::$post[$placeholder-1],$requestURI,$Route->arguments];
        }
        else
        {
            return false;
        }
    }

    static function int($num)
    {
        if(is_integer((int)$num) && strlen($num) == strlen((int)$num))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    static function number()
    {

    }

    static function someLetter()
    {

    }

    static function isPost()
    {
        if($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            return true;
        }

        return false;
    }

    public function returnGet()
    {
        return self::$get;
    }
}
