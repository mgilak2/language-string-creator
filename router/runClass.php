<?php

class Run{


   public $customRun = false;

    public function customRun($ioc,$route){

        $this->customRun = true;


            if(@$ioc[$route[0][1]["run"]]){
                $keyname = $route[0][1]["run"];
                $classPath = $ioc[$keyname][0];
                include_once $classPath;
                $className = $ioc[$keyname][1];
                $obj = new $className;

                if(method_exists($obj,"start")){
                    $routeArgument = $route[2];
                    $obj->start($routeArgument);
                }else{
                    echo "no such a function exsit , function name is : start";
                }

            }else{
                $keyname = $route[0][1]["run"];
                $className =$ioc[$keyname][1];
                echo "no such a class exsit , class name is : ".$className;
            }

        }


    public function  befor($route){
            if($route[0][1]["beforFilter"] !== null){
                $routeArgument = $route[2];
            $route[0][1]["beforFilter"]($routeArgument);

            }
    }

    public function __function(&$route){
        if($route[0][1]["function"] !== null){
            $routeArgument = $route[2];
            $function = $route[0][1]["function"];
            $function($routeArgument);

            if($route[0][1]["controller"] !== null){
                $route[0][1]["controller"] = null;
            }

        }
    }


    public function controller($route){
        if($route[0][1]["controller"] !== null){

            $controllerParts = explode("@",$route[0][1]["controller"]); // this one exploding for example Hasan@index

            //from array [Hasan@index,['args'=>...]]
            $iocKeyName = $controllerParts[0];
            if(@$iocKeyName){
                $classPath = $controllerParts[0];
                $className = $controllerParts[0];
                $obj = new $className;
                $methodName = $controllerParts[1];
                if(method_exists($obj,$methodName))
                {
                    $routeArgument = $route[2];
                    $controllerArguments = $route[0][1]["controller"][1];
                    $obj->$methodName($routeArgument,$controllerArguments);
                }

                else
                {
                    echo "no such a function exsit , function name is : ".$methodName;
                }

            }
            else
            {
                $className = $controllerParts[0];
                echo "no such a class exsit , class name is : ".$className;
            }

        }
    }

    public function  after($route){
        if($route[0][1]["afterFilter"] !== null){
            $route[0][1]["afterFilter"]($route[2]);

        }
    }

    public function isCustomRun($route){
        if($route[0][1]["run"] !== null){
            return true;
        }
        return false;
    }


}


$Run = new Run;