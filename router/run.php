<?php
$Route = new Route;
$Run = new Run;

include "../app/route.php";

if(!$Route::isPost())
{
    // get header request
    if($route = $Route::matchGetRoute($Route)){
        $Run->befor($route);
        $Run->__function($route);
        $Run->controller($route);
    }
    else
    {
        echo "404 error get route";
    }
}
else{
    // post header
    if($route = $Route::matchPostRoute($Route))
    {
        $Run->befor($route);
        $Run->__function($route);
        $Run->controller($route);
    }
    else
    {
        echo "404 error post route";
    }
}

